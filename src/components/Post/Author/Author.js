// @flow strict
import React from 'react';
import { Link } from 'gatsby';
import styles from './Author.module.scss';

const Author = () => (
    <div className={styles['author']}>
      <p className={styles['author__bio']}>
        If you have an enquiry regarding the material published above, 
        or in relation to the treatment of cryptoassets under the law of England and Wales,
        please find our contact details
        <span> </span>
        <Link
          className={styles['author__bio-twitter']}
          to='/pages/contact'
          rel='noopener noreferrer'
          target='_blank'
        >
          here
        </Link>.
      </p>
    </div>
);

export default Author;
