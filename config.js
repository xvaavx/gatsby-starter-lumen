'use strict';

module.exports = {
  url: 'https://cryptoassets.law/',
  pathPrefix: '/',
  title: 'Blog by cryptoassets.law',
  subtitle: 'Information regarding cryptoassets under the law of England and Wales.',
  copyright: '© All rights reserved.',
  disqusShortname: '',
  postsPerPage: 4,
  googleAnalyticsId: '',
  useKatex: false,
  menu: [
    {
      label: 'Articles',
      path: '/'
    },
    {
      label: 'Legal Statements',
      path: '/pages/legal-statements'
    },
    {
      label: 'Contact',
      path: '/pages/contact'
    },
  ],
  author: {
    name: 'cryptoassets.law',
    photo: '/photo.png',
    bio: 'Information regarding the treatment of cryptoassets under the law of England and Wales.',
    contacts: {
      email: '',
      facebook: 'Paradigm-Legal-102354738657620',
      telegram: 'paradigmlegal',
      twitter: 'paradigm_legal',
      github: '',
      rss: '',
      vkontakte: '',
      linkedin: '',
      instagram: '',
      line: '',
      gitlab: '',
      weibo: '',
      codepen: '',
      youtube: '',
      soundcloud: '',
      medium: '',
    }
  }
};
