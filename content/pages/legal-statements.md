---
template: page
title: Legal Statements
slug: legal-statements
socialImage: /media/image-3.jpg
draft: false
---
### A. DISCLAIMER

The information published on this website is for general information purposes only and does not or is not intended to constitute legal or other professional advice or an opinion of any kind. Users of this website are advised to seek legal advice in respect to any specific legal issues. No warrant or guarantee is given as to the quality, accuracy or completeness of any information published on this website.

This website may contain links to third-party websites; any such information contained on those websites is not recommended or endorsed. No reader, user or browser of this website should act or omit from acting on the basis of information published on this website without first seeking legal advice in the relevant jurisdiction. Use of or access to this website does not create a lawyer-client relationship between the reader user or browser and any entity connected to this website.

### B. TERMS AND CONDITIONS OF USE

#### 1. About our terms

1.1 These terms and conditions of use (Terms) explain how you may use this website and any of its content (Site). These Terms apply between Paradigm Legal Ltd (we, us or our) and you, the person accessing or using the Site (you or your).

1.2 You should read these Terms carefully before using the Site. By using the Site or otherwise indicating your consent, you agree to be bound by these Terms. If you do not agree with any of these Terms, you should stop using the Site immediately.

1.3 The Site is provided by us to you free of charge for information purposes only.

#### 2. About us

2.1 We are Paradigm Legal Ltd, a company registered in England and Wales under company registration number 12083387.

2.2 If you have any questions about the Site, please contact us by sending an email to mail@paradigmlegal.com.

#### 3. Using the site

3.1 The Site is for your personal and non-commercial use only.

3.2 You agree that you are solely responsible for all costs and expenses you may incur in relation to your use of the Site.

3.3 We make no promise that the Site is appropriate or available for use in locations outside of the UK. If you choose to access the Site from locations outside the UK, you acknowledge you do so at your own initiative and are responsible for compliance with local laws where they apply.

3.4 As a condition of your use of the Site, you agree to comply with our Acceptable Use Policy (shown below) and agree not to:

3.4.1 misuse or attack our Site by knowingly introducing viruses, trojans, worms, logic bombs or any other material which is malicious or technologically harmful (such as by way of a denial-of-service attack), or

3.4.2 attempt to gain unauthorised access to our Site, the server on which our Site is stored or any server, computer or database connected to our Site.

3.4.3 We may prevent or suspend your access to the Site if you do not comply with these Terms or any applicable law.

#### 4. Your privacy and personal information

Your privacy and personal information are important to us. Any personal information that you provide to us will be dealt with in line with our Privacy Policy, which explains what personal information we collect from you, how and why we collect, store, use and share such information, your rights in relation to your personal information and how to contact us and supervisory authorities in the event you have a query or complaint about the use of your personal information.

#### 5. Ownership, use and intellectual property rights

5.1 The intellectual property rights in the Site and in any text, images, video, audio or other multimedia content, software or other information or material submitted to or accessible from the Site (Content) are owned by us and our licensors.

5.2 We and our licensors reserve all our intellectual property rights (including, but not limited to, all copyright, trade marks, domain names, design rights, database rights, patents and all other intellectual property rights of any kind) whether registered or unregistered anywhere in the world. This means, for example, that we remain owners of them and are free to use them as we see fit.

5.3 Nothing in these Terms grants you any legal rights in the Site or the Content other than as necessary for you to access it. You agree not to adjust, try to circumvent or delete any notices contained on the Site or the Content (including any intellectual property notices) and in particular, in any digital rights or other security technology embedded or contained within the Site or the Content.

#### 6. Submitting information to the site

6.1 While we try to make sure that the Site is secure, we do not actively monitor or check whether information supplied to us through the Site is confidential, commercially sensitive or valuable.

6.2 Other than any personal information which will be dealt with in accordance with our Privacy Policy, we do not guarantee that information supplied to us through the Site will be kept confidential and we may use it on an unrestricted and free-of-charge basis as we reasonably see fit.

#### 7. Accuracy of information and availability of the site

7.1 We try to make sure that the Site is accurate, up-to-date and free from bugs, but we cannot promise that it will be. Furthermore, we cannot promise that the Site will be fit or suitable for any purpose. Any reliance that you may place on the information on the Site is at your own risk.

7.2 We may suspend or terminate access or operation of the Site at any time as we see fit.

7.3 Any Content is provided for your general information purposes only and to inform you about us and our products and news, features, services and other websites that may be of interest, but has not been tailored to your specific requirements or circumstances. It does not constitute technical, financial or legal advice or any other type of advice and should not be relied on for any purposes. You should always use your own independent judgment when using our Site and its Content.

7.4 While we try to make sure that the Site is available for your use, we do not promise that the Site will be available at all times or that your use of the Site will be uninterrupted.

#### 8. Hyperlinks and third party sites

The Site may contain hyperlinks or references to third party advertising and websites other than the Site. Any such hyperlinks or references are provided for your convenience only. We have no control over third party advertising or websites and accept no legal responsibility for any content, material or information contained in them. The display of any hyperlink and reference to any third party advertising or website does not mean that we endorse that third party’s website, products or services. Your use of a third party site may be governed by the terms and conditions of that third-party site and is at your own risk.

#### 9. Limitation on our liability

9.1 Except for any legal responsibility that we cannot exclude in law (such as for death or personal injury) or arising under applicable laws relating to the protection of your personal information, we are not legally responsible for any:

9.1.1 losses that were not foreseeable to you and us when these Terms were formed;

9.1.2 losses that were not caused by any breach on our part;

9.1.3 business losses; and

9.1.4 losses to non-consumers.

#### 10. Events beyond our control

We are not liable to you if we fail to comply with these Terms because of circumstances beyond our reasonable control.

#### 11. Rights of third parties

No one other than a party to these Terms has any right to enforce any of these Terms.

#### 12. Variation

12.1 No changes to these Terms are valid or have any effect unless agreed by us in writing or made in accordance with this clause.

12.2 We reserve the right to vary these Terms from time to time. Our updated Terms will be displayed on the Site and by continuing to use and access the Site following such changes, you agree to be bound by any variation made by us. It is your responsibility to check these Terms from time to time to verify such variations.

#### 13. Disputes

13.1 We will try to resolve any disputes with you quickly and efficiently. If you are unhappy with us, please contact us as soon as possible using the contact details set out at the top of this page.

13.2 Relevant England and Wales law will apply to these Terms. If you want to take court proceedings, the relevant courts of England and Wales will have non-exclusive jurisdiction in relation to these Terms.

### C. ACCEPTABLE USE POLICY

#### 1. About this policy

1.1 Together with our Terms, this acceptable use policy (Policy) governs how you may access and use this website and all associated web pages (Site).

1.2 You should read this Policy carefully before using the Site.

1.3 By accessing or using the Site or otherwise indicating your consent, you agree to be bound by this Policy, which supplements our Terms and Conditions of Use. If you do not agree with or accept any part of this Policy, you should stop using the Site immediately.

1.4 If you have any questions about this Policy, please contact us using the contact details provided in our Terms.

1.5 If you would like this Policy in another format (for example: audio, large print, braille) please contact us using the contact details provided in our Terms.

1.6 In this Policy, ‘we’, ‘us’ or ‘our’ means Paradigm Legal Ltd, company registration number 12083387; and ‘you’ or ‘your’ means the person accessing or using the Site or its content.

#### 2. Acceptable use

We permit you to use the Site only for personal, non-commercial purposes. Use of the Site in any other way, including any unacceptable use set out in this Policy, is not permitted.

#### 3. Unacceptable use

3.1 As a condition of your use of the Site, you agree not to use the Site:

3.1.1 for any purpose that is unlawful under any applicable law or prohibited by this Policy or our Terms;

3.1.2 to commit any act of fraud;

3.1.3 to distribute viruses or malware or other similar harmful software code;

3.1.4 for purposes of promoting unsolicited advertising or sending spam;

3.1.5 to simulate communications from us or another service or entity in order to collect identity information, authentication credentials, or other information (‘phishing’);

3.1.6 in any manner that disrupts the operation of our Site or business or the website or business of any other entity;

3.1.7 in any manner that harms minors;

3.1.8 to promote any unlawful activity;

3.1.9 to represent or suggest that we endorse any other business, product or service unless we have separately agreed to do so in writing;

3.1.10 to gain unauthorised access to or use of computers, data, systems, accounts or networks; or

3.1.11 to attempt to circumvent password or user authentication methods.

#### 4. Bulletin boards, chat rooms and other interactive services

4.1 We may make bulletin boards, chat rooms or other communication services (Interactive Services) available on the Site.

4.2 We are not obliged to monitor or moderate any text, images, video, audio or other multimedia content, information or material (Submission) submitted to our Interactive Services. Where we do monitor or moderate Submissions we shall indicate how this is performed and who should be contacted in relation to any Submission of concern to you.

4.3 We may remove or edit any Submission to any of our Interactive Services whether they are moderated or not.

4.4 Any Submission you make must comply with our Submission standards set out in clause V below.

4.5 By making a Submission, you grant to us a royalty-free, irrevocable, non-exclusive, transferable licence to use, reproduce, modify, publish, edit, translate, distribute, perform and display the Submission (in whole or in part) on the Site, and on any other websites operated by us, indefinitely.

#### 5. Submission standards

5.1 Any Submission you make to our Interactive Services and any other communication to users of our Site by you must conform to standards of accuracy, decency and lawfulness, which shall be applied in our discretion, acting reasonably.

5.2 In particular, any Submission or communication by you must be:

5.2.1 your own original work and lawfully submitted;

5.2.2 factually accurate or your own genuinely held belief;

5.2.3 provided with the necessary consent of any third party;

5.2.4 not defamatory or likely to give rise to an allegation of defamation;

5.2.5 not offensive, obscene, sexually explicit, discriminatory or deceptive; and

5.2.6 unlikely to cause offence, embarrassment or annoyance to others.

#### 6. Linking and framing

6.1 You may create a link to our Site from another website without our prior written consent provided no such link:

6.1.1 creates a frame or any other browser or border environment around the content of our Site;

6.1.2 implies that we endorse your products or services or any of the products or services of, or available through, the website on which you place a link to our Site;

6.1.3 displays any of the trade marks or logos used on our Site without our permission or that of the owner of such trade marks or logos; or

6.1.4 is placed on a website that itself breaches this Policy.

6.2 We reserve the right to require you to immediately remove any link to the Site at any time, and you shall immediately comply with any request by us to remove any such link.

#### 7. Using our name and logo

You may not use our trade marks, logos or trade names except in accordance with this Policy and our Terms.

#### 8. Breach

We shall apply the terms of this Policy in our absolute discretion. In the event of your breach of this Policy we may terminate or suspend your use of the Site, remove or edit Submissions, disclose Submissions or any other communication to users of our Site by you to law enforcement authorities or take any action we consider necessary to remedy the breach.

### D. PRIVACY POLICY

Our Site is provided by Paradigm Legal Ltd, a company registered in England and Wales under company registration number 12083387 (‘we’, ‘our’ or ‘us’). We are the controller of personal data obtained via our website, meaning we are the organisation that is legally responsible for deciding how and for what purposes it is used.

We take your privacy very seriously. Please read this privacy policy carefully as it contains important information on who we are and how and why we collect, store, use and share any information relating to you (your personal data) in connection with your use of our website. It also explains your rights in relation to your personal data and how to contact us or a relevant regulator in the event you have a complaint.

We collect, use and are responsible for certain personal data about you. When we do so we are subject to the UK General Data Protection Regulation (UK GDPR). We are also subject to the EU General Data Protection Regulation (EU GDPR) in relation to goods and services we offer to individuals in the European Economic Area (EEA).

This privacy policy is divided into the following sections:

* What this policy applies to
* Personal data we collect about you
* How your personal data is collected
* How and why we use your personal data
* Marketing
* Who we share your personal data with
* How long your personal data will be kept
* Transferring your personal data out of the UK and EEA
* Cookies and other tracking technologies
* Your rights
* Keeping your personal data secure
* How to complain
* Changes to this privacy policy
* How to contact us

#### What this policy applies to

This privacy policy relates to your use of our website only.

Throughout our website we may link to other websites owned and operated by certain third parties. Those third party websites may also gather information about you in accordance with their own separate privacy policies. For privacy information relating to those third party websites, please consult their privacy policies as appropriate.

#### Personal data we collect about you

The personal data we collect about you depends on the particular activities carried out through our website. We may collect and use some or all of the following personal data about you:

* your name, address and contact information, including email address and telephone number and company details
* information to check and verify your identity, eg date of birth
* your gender, if you choose to give this to us
* location data, if you choose to give this to us
* your billing information, transaction and payment card or other payment method information
* bank account and payment details
* details of any information, feedback or other matters you give us by phone, email, post or via social media
* your account details, such as username and login details
* your activities on, and use of, our website
* your professional online presence, eg LinkedIn profile
* information about the services we provide to you
* your contact history, purchase history and saved items
* information about how you use our website and technology systems

If you do not provide personal data we ask for where it is reasonably required by us, it may delay or prevent us from providing products and services to you.

We collect and use this personal data for the purposes described in the section ‘How and why we use your personal data’ below.

#### How your personal data is collected

We collect personal data from you:

* directly, when you enter or send us information, and
* indirectly, such as your browsing activity while on our website; we will usually collect information indirectly using the technologies explained in the section on ‘Cookies and other tracking technologies’ below

#### How and why we use your personal data

Under data protection law, we can only use your personal data if we have a proper reason, eg:

* where you have given consent
* to comply with our legal and regulatory obligations
* for the performance of a contract with you or to take steps at your request before entering into a contract, or
* for our legitimate interests or those of a third party

A legitimate interest is when we have a business or commercial reason to use your information, so long as this is not overridden by your own rights and interests.

See ‘Who we share your personal data with’ for further information on the steps we will take to protect your personal data where we need to share it with others.

#### Marketing

We may use your personal data to send you updates (by email, text message, telephone or post) about our products and services, including exclusive offers, promotions or new products and services.

We have a legitimate interest in using your personal data for marketing purposes (see above ‘How and why we use your personal data’). This means we do not usually need your consent to send you marketing information. However, where consent is needed, we will ask for this separately and clearly.

You have the right to opt out of receiving marketing communications at any time by contacting us by email at mail@paradigmlegal.com.

We will always treat your personal data with the utmost respect and never sell or share it with other organisations for marketing purposes.

For more information on your right to object at any time to your personal data being used for marketing purposes, see ‘Your rights’ below.

#### Who we share your personal data with

We may share personal data with:

* third parties we use to help deliver our products and services to you, eg payment service providers, warehouses and delivery companies

* other third parties we use to help us run our business, eg marketing agencies or website hosts and website analytics providers

We or the third parties mentioned above may occasionally also need to share personal data with:

* external auditors, eg in relation to the audit of our accounts, in which case the recipient of the information will be bound by confidentiality obligations

* professional advisors (such as lawyers and other advisors), in which case the recipient of the information will be bound by confidentiality obligations

* law enforcement agencies, courts, tribunals and regulatory bodies to comply with our legal and regulatory obligations

* other parties in connection with a significant corporate transaction or restructuring, including a merger, acquisition, asset sale, initial public offering or in the event of our insolvency—usually, information will be anonymised but this may not always be possible, however, the recipient of the information will be bound by confidentiality obligations

#### How long your personal data will be kept

We will not keep your personal data for longer than we need it for the purpose for which it is used.

#### Transferring your personal data out of the UK and EEA

The EEA, UK and other countries outside the EEA and the UK have differing data protection laws, some of which may provide lower levels of protection of privacy. 

It is sometimes necessary for us to share your personal data to countries outside the UK and EEA. In those cases we will comply with applicable UK and EEA laws designed to ensure the privacy of your personal data.

#### Cookies and other tracking technologies

A cookie is a small text file which is placed onto your device (eg computer, smartphone or other electronic device) when you use our website. We may use cookies on our website.

For further information on cookies, please see our Cookie Policy.

#### Your rights

You generally have the following rights, which you can usually exercise free of charge. A summary of these rights can be found on the UK’s Information Commissioner website, at https://ico.org.uk/your-data-matters.

If you would like to exercise any of your rights, please email us—see below: ‘How to contact us’. When contacting us please provide enough information to identify yourself, and let us know which right(s) you want to exercise and the information to which your request relates.

#### Keeping your personal data secure

We have appropriate security measures to prevent personal data from being accidentally lost, or used or accessed unlawfully. We limit access to your personal data to those who have a genuine business need to access it.

We also have procedures in place to deal with any suspected data security breach. We will notify you and any applicable regulator of a suspected data security breach where we are legally required to do so.

#### How to complain

Please contact us if you have any queries or concerns about our use of your information (see below ‘How to contact us’). We hope we will be able to resolve any issues you may have.

You also have the right to lodge a complaint with the Information Commissioner in the UK, and a relevant data protection supervisory authority in the EEA state of your habitual residence, place of work or of an alleged infringement of data protection laws in the EEA.

#### Changes to this privacy policy

We may change this privacy policy from time to time—when we make significant changes we will endeavour to take steps to inform you.

#### How to contact us

You can contact us by email if you have any questions about this privacy policy or the information we hold about you, to exercise a right under data protection law or to make a complaint. Our email address is mail@paradigmlegal.com.

### E. COOKIE POLICY

Please read this cookie policy carefully as it contains important information on who we are and how we use cookies on our website. This policy should be read together with our Privacy Policy which sets out how and why we collect, store, use and share personal information generally, as well as your rights in relation to your personal information and details of how to contact us and supervisory authorities if you have a complaint.

#### Our website

This cookie policy only relates to your use of our Site. 

#### Cookies

A cookie is a small text file which is placed onto your device (eg computer, smartphone or other electronic device) when you use our website. We may use cookies on our website. These help us to recognise you and your device and store some information about your preferences or past actions.

For example, we may monitor how many times you visit the website, which pages you go to, traffic data, location data and the originating domain name of your internet service provider. This information helps us to build a profile of our users.

For further information on our use of cookies, including a detailed list of your information which we and others may collect through cookies, please see below.

For further information on cookies generally, including how to control and manage them, visit the guidance on cookies published by the UK Information Commissioner’s Office, www.aboutcookies.org or www.allaboutcookies.org.

#### Consent to use cookies and changing settings

We will ask for your consent to place cookies or other similar technologies on your device, except where they are essential for us to provide you with a service that you have requested.

#### How to turn off all cookies and consequences of doing so

If you do not want to accept any cookies, you may be able to change your browser settings so that cookies (including those which are essential to the services requested) are not accepted. If you do this, please be aware that you may lose some of the functionality of our website.

For further information about cookies and how to disable them please go to the guidance on cookies published by the UK Information Commissioner’s Office, www.aboutcookies.org or www.allaboutcookies.org.

#### How to contact us

Please contact us if you have any questions about this cookie policy or the information we hold about you.
If you wish to contact us, please send an email to mail@paradigmlegal.com.
