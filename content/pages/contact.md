---
template: page
title: Contact
slug: contact
socialImage: /media/cpu.svg
draft: false
---
In relation to any queries, please contact us by email, at mail@paradigmlegal.com