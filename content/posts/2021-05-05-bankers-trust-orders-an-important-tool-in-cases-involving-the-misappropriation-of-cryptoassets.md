---
template: post
title: Bankers Trust orders - an important tool in cases involving
  cryptocurrency fraud
slug: bankers-trust-orders
socialImage: /media/image-3.jpg
draft: false
date: 2021-05-05T16:16:47.420Z
description: A Bankers Trust order is a type of injunctive remedy of the court
  which requires a financial institution to provide information and documents in
  respect of a customer who has perpetrated a fraud.
category: Civil Fraud
tags:
  - Civil Fraud
---
A Bankers Trust order is a type of injunctive remedy of the court which requires a financial institution to provide information and documents in respect of a customer who has perpetrated a fraud.

Unfortunately, the prevalence of financial crime is increasing at an exponential rate both in the UK and elsewhere. Bankers Trust orders provide a potentially effective means to ascertain the identity of perpetrators of fraud in cases involving the misappropriation of cryptocurrency through cryptocurrency exchanges. In this article we consider the application of the remedy in this context.

### Case law

The remedy arises from the case of *Bankers Trust Co v Shapira and Others* \[1980] 1 WLR 1274. The facts of the case were as follows. In 1979 two men presented cheques amounting to $1 million, from a bank in Saudi Arabia, to a New York bank. The cheques were honoured and on the instructions of the men the funds were subsequently transferred from the New York bank to another bank in London. Some time later, the Saudi Arabia bank, from where the funds originated, discovered that the cheques were forgeries, which led to the New York bank being required to reimburse the money.

The New York bank brought proceedings in the Commercial Court in London and sought orders against the London bank, which included an order for disclosure and copies of:

* all correspondence between the two men and the London bank relating to any account in either of the two men's names;
* all cheques drawn on such accounts, and
* all debit vouchers, transfer applications and orders and internal memoranda relating to any account standing in the names of either of the two men at the London bank.

On appeal, the court granted these orders.

The equitable jurisdiction of the court to make a Bankers Trust order has evolved through subsequent case law. In *Miles Smith Broking Ltd v Barclays Bank plc* \[2017] EWHC 3338 (Ch), the High Court extended the scope of persons who may apply for an order to a trustee which was not the beneficial owner of the funds of which recovery was sought.

In relation to cryptoassets, in December 2020 the High Court granted a Bankers Trust order in the case of *Ion Science Ltd and Duncan Johns v Persons Unknown, Binance Holdings Limited and Payward Limited* (unreported, 21 December 2020).

The order required the Binance and Kraken cryptocurrency exchanges to disclose information about the true identity of individuals who had perpetrated a fraud by misappropriating more than £577,000 from a businessman. The fraud involved the consensual purchase of Bitcoin in that amount on the Coinbase cryptocurrency exchange, and the subsequent misappropriation of the Bitcoin by the fraudsters, by way of transfer of the Bitcoin to Binance and Kraken.

We have covered the *Ion Science* case in a separate post which may be accessed using this [link](https://www.cryptoassets.law/posts/ion-science-and-persons-unknown).

### Obtaining a Bankers Trust order

The considerations to which the court will have regard upon determination of an application for a Bankers Trust order, in relation to a cryptocurrency exchange, are as follows:

* Whether there is shown to be good grounds for concluding that the cryptocurrency in respect of which an order for disclosure is sought belongs to the claimant.
* Whether there is a real prospect that the information sought will lead to the location or preservation of the cryptocurrency.
* The order should not be wider than necessary (i.e. in respect to the scope of the information or documents sought by way of disclosure).
* The interests of the applicant in obtaining an order have to be balanced against any detriment to the respondent.
* The applicant should provide undertakings only to use the information and documents for the purpose specified (i.e., to pursue recovery of cryptocurrency from the perpetrators of fraud).
* The applicant should pay the reasonable expenses of the cryptocurrency exchange(s) to provide the information.
* The applicant should provide a cross-undertaking in damages in order to compensate the respondent for loss incurred in the event that it is later shown that the order should not have been made.

We address various of these grounds in further detail below.

### Inquiries and evidence

In order to obtain a Bankers Trust order, it is necessary to satisfy the court by way of evidence that cryptocurrencies held by an exchange belong to the applicant, and that the applicant has been fraudulently deprived of them.

In cases involving the misappropriation of funds from cryptocurrency wallets, transactions may be traced on the blockchain to ascertain if funds have been transferred to a cryptocurrency exchange (from where it may be converted into ordinary, or ‘fiat’ currency, such as pound sterling).

A specialist IT consulting firm (which may be variously described as an IT security, blockchain analytics or blockchain analysis firm) will need to be instructed in order to gather evidence and undertake appropriate analysis. This important step will provide insight into the merits of any potential application for injunctive relief. If it is revealed that cryptocurrencies have been transferred to an exchange, the IT firm can produce a report that may be presented to the court as evidence in support of an application.

In many instances, analysis of the blockchain is likely to reveal that misappropriated cryptocurrencies have not been transferred to a (centralized) exchange, but instead to a private wallet. In other instances, cryptocurrencies may have been transferred to a decentralized exchange which provides for direct peer-to-peer transactions without a central authority. In these cases, a Bankers Trust order will be redundant since there is not any institution to provide disclosure in a decentralized blockchain network.

However, an IT firm may reveal other intelligence which leads to a train of enquiry that facilitates the potential recovery of assets. For instance, most cryptocurrencies including Bitcoin are pseudonymous (although some are anonymous), and there may be a possibility of tracing transactions to an individual's identity - perhaps through IP addresses or exchanges associated with transactions. That said, it should be appreciated that sophisticated perpetrators are likely to take steps to obfuscate cryptocurrency transactions through the use of techniques known as ‘mixing’, ‘CoinJoins’, and ‘chain-hopping’.

### Target documents and information

Cryptocurrency exchanges are required to undertake appropriate anti-money laundering compliance on their customers. In the UK, businesses carrying on cryptoasset activities are required to comply with the *Money Laundering, Terrorist Financing and Transfer of Funds (Information on the Payer) Regulations 2017*. This legislation requires businesses to sufficiently identify the beneficial owners of cryptoassets by undertaking “know your customer” (‘KYC’) and customer due diligence (‘CDD’) checks.

It is therefore to be expected that cryptocurrency exchanges will hold information and documents in respect of their customers, which may include (but will not be limited to) the following:

* Copies of personal identification documents, such as a passport or driving licence.
* Copies of address verification documents, such as a utility bill.
* Video verification of identity (which may provide a useful means of information in the context of modern technological advancements).
* Information in respect to third-party bank accounts held by customers.
* IP address logs, which may reveal the physical location of a customer.

### Cross-undertaking in damages

Injunctive relief is an interim remedy of the court that is granted before a full trial has taken place. At the interim stage the court does not have the opportunity to assess the full merits of the case. For this reason, when making applications for injunctive relief, such as for a Bankers Trust order, the applicant is required to provide a cross-undertaking in damages. This is a promise to compensate the respondent if it is later determined by the court that the applicant was not entitled to the relief granted. The court may also require the applicant to provide security for the undertaking in damages (for example, by paying money to be held in court).

The implications of a later decision of the court to set aside an order for injunctive relief present an appreciable financial risk to applicants, and should be carefully borne in mind prior to embarking on an application to court.

### Costs

In addition to providing a cross-undertaking in damages, the claimant will be required to meet the reasonable expenses of the cryptocurrency exchange in providing the disclosure sought. It is not generally anticipated that such costs will be significant but this will depend on the scope of the information that is sought.

### Timing

In applications for injunctive relief involving the misappropriation of cryptocurrencies it will be necessary to act on an urgent basis, because the nature of cryptocurrencies is that they can be dissipated at any moment (or, as the court has put it, “at the click of a mouse”).

## Conclusion

Cases involving the misappropriation of cryptocurrencies present a substantial set of difficulties at the nexus of law and computing. However, a Bankers Trust order is an important tool that may reveal the identity of the perpetrators of fraud and facilitate the recovery of assets.

*cryptoassets.law*