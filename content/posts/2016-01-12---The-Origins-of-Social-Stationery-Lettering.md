---
template: post
title: Ion Science Ltd and Duncan Johns v Persons Unknown, Binance Holdings
  Limited and Payward Limited – unreported, 21 December 2020
slug: ion-science-and-persons-unknown
socialImage: /media/image-3.jpg
draft: false
date: 2021-04-25T20:41:17.187Z
description: A case in which the applicants sought interim remedies of the court
  following the misappropriation of Bitcoin.
category: Civil Fraud
tags:
  - Civil Fraud
  - Civil Claims
---
In this case which came before the Commercial Court, the applicants, Ion Science Ltd and Duncan Johns, sought interim remedies in respect to the misappropriation by ‘persons unknown’ of 64.35 Bitcoin (converted from approximately 577,002 GBP).

Mr Johns claimed that a fictitious Swiss company known as Neo Capital induced him to transfer the misappropriated Bitcoin on the basis he would receive profits of at least £15 million, through the purchase of initial coin offerings (ICOs) of cryptocurrencies known as Uvexo and Oileum.

In their court application, the applicants sought to recover the misappropriated Bitcoin by discovering the true identity of *persons unknown* and securing their assets. The information necessary to reveal the fraudsters’ identity was sought from the Binance and Kraken cryptocurrency exchanges (to which blockchain analysis evidence revealed that some or all of the Bitcoin had been transferred).

The legal claims which formed the basis of the application arose in deceit, unlawful means conspiracy, and by way of an equitable proprietary claim in respect to the Bitcoin (or its traceable proceeds). A proprietary interest is a legal interest which attaches to property itself - in this case, Bitcoin.

The court granted a proprietary injunction, a worldwide freezing order, an ancillary disclosure order against persons unknown, and a Bankers Trust order against Binance and Kraken. An injunction is a mandatory order of the court which requires a party to do or refrain from doing a specific act, and a freezing order is intended to prevent a party from disposing of assets prior to a final hearing. Bankers Trust orders are covered below.

### Facts

The relevant facts are understood to be as follows:

* Mr Duncan Johns, the director of Ion Science Ltd, was approached by one or more persons who claimed to be specialist advisors at a Swiss investment firm known as Neo Capital. These persons included a senior associate at Neo Capital known as ‘Ms Black’.
* Ms Black persuaded Mr Johns to make initial investments into Ethereum and Dimecoin. In order to arrange the investments, Mr Johns provided remote access to his computer. It would appear that Ms Black accessed the computer and arranged to transfer money from Mr Johns’ personal bank account into his Coinbase account, and then make conversions of the fiat currency into Bitcoin. 
* The Ethereum and Dimecoin investments were successful and produced a return. As a result of this Mr Johns was persuaded to make a second investment into two ICOs; Uvexo and Oileum.
* Mr Johns decided to make the second investment using funds obtained from Ion Science Ltd. He again provided remote access to his computer for the relevant arrangements to be made.
* Several months later, Ms Black informed Mr Johns that profits in the region of £15 million were due to him from the Oileum investment, but that commission needed to be paid in order to release the profits. Mr Johns agreed, and again provided Ms Black with remote access his computer.
* The profits did not transpire. Mr Johns discovered that Neo Capital was not a real company; it was not on the Swiss register of companies and the Swiss regulator had issued a warning that it may be providing unauthorised services. Neo Capital did not have any presence on the internet.
* Neo Capital became unresponsive. Expert evidence (blockchain analysis) later revealed that a substantial part of the Bitcoin was held in accounts on Binance and Kraken.

### Bankers Trust order

A Bankers Trust order is a type of third party disclosure order of the court, which requires a bank or financial institution to disclose confidential information or documents in order to support a claim to trace assets. It is named after the case of *Bankers Trust Company v Shapira and others* \[1980] 1 WLR 1274.

The Ion Science case is said to be the first case in which the court has granted a Bankers Trust order against a cryptocurrency exchange domiciled outside of the jurisdiction. In a similar previous case involving cryptocurrency fraud, *AA v Persons Unknown who demanded Bitcoin on 10th and 11th October 2019*, the court had been reluctant to grant such an order.

### Lex situs

An important issue arose in the case in regards to the governing law of the cryptoassets (Bitcoin), concerning a legal issue known as lex situs.

Lex situs is a latin term which means the law of the country where property is situated at the time it is transferred. It has previously arisen in banking and finance law, and aviation law, but there have not been any prior cases in relation to cryptoassets.

In the Ion Science case, the court gave guidance that there was a serious issue to be tried that the lex situs of a cryptoasset is the place where the person or company who owns it is domiciled. In this regard the court relied on views expressed in an academic publication by Professor Andrew Dickinson in his book *[Cryptocurrencies in Public and Private Law](https://global.oup.com/academic/product/cryptocurrencies-in-public-and-private-law-9780198826385?cc=gb&lang=en&)*.

Other relevant factors included that the damage (misappropriation) occurred in England, and that is where the cryptoassets were located before they were transferred.

*cryptoassets.law*