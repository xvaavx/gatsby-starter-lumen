---
template: post
title: The publication of the Digital Dispute Resolution Rules in April 2021
slug: the-digital-dispute-resolution-rules
socialImage: /media/image-3.jpg
draft: false
date: 2021-05-04T14:45:13.274Z
description: The Digital Dispute Resolution Rules are intended to provide a
  rapid arbitration process for the resolution of disputes arising from novel
  digital technologies such as cryptoassets.
category: Dispute Resolution
tags:
  - Dispute Resolution
---
In April 2021, a taskforce established by UK government and formed of legal and industry experts, published a set of digital dispute resolution rules, which are intended to provide a mechanism for the rapid and cost effective resolution of disputes arising from novel digital technologies such as cryptoassets.

### The UK Jurisdiction Taskforce

The UK Jurisdiction Taskforce (‘UKJT’) is a taskforce of the LawTech Delivery Panel, and is a team of technology and legal industry experts and leading figures from UK government and the judiciary.

In its literature, the UKJT states that its establishment is intended to demonstrate that English law and the jurisdiction of England and Wales together provide a ‘state-of-the-art foundation’ for the development of distributed ledger technology, smart contracts and associated technologies.

In November 2019 the UKJT published a legal statement which, while not legally binding, set out that cryptoassets should be treated as property under English common law, and that smart contracts are capable of satisfying the basic requirements of an English law contract. The guidance in the legal statement has been followed in several decisions of the courts of England and Wales in relation to cryptoassets.

### The Digital Dispute Resolution Rules

On 22 April 2021, the UKJT published the Digital Dispute Resolution Rules (‘the Rules’). The stated purpose of the Rules is to facilitate the ‘rapid’ and ‘cost effective’ resolution of commercial disputes involving novel digital technology such as cryptoassets, cryptocurrency, smart contracts, distributed ledger technology, and fintech applications.

The Rules, which may be accessed here, have been prepared in consultation with lawyers, technical experts and financial services and commercial parties (many of whom are external to the UKJT team).

#### Arbitration

Disputes under the Rules are generally intended to be resolved by submission to arbitration, pursuant to the terms of the Arbitration Act 1996. However, the Rules may be varied at the choice of the parties to allow for a dispute to instead be resolved by a process known as expert determination (further discussed below).

Arbitration is an alternative means of resolving a dispute to court based litigation. The basis of arbitration is contractual; in this sense the process is based on an agreement of the parties, and it provides them with a choice as to where the arbitration takes place, and the procedure which governs it. Unlike proceedings in court, arbitration proceedings are usually confidential.

Decisions made by an arbitral tribunal are generally final and not subject to a right of appeal. Arbitral awards are widely enforceable on a global basis, pursuant to the New York Convention. Over 165 countries are party to this treaty and have agreed to uphold an agreement to arbitrate and recognise and enforce foreign arbitral awards in their respective legal jurisdictions.

#### Expert determination

An alternative means of resolving disputes under the Rules is by expert determination, which involves the making of a determination by an independent third party. The expert determination process differs from arbitration in key respects, since it does not involve the use of the Arbitration Act 1996 legal framework, and determinations are not internationally enforceable in the same way as arbitral awards.

#### Scope and applicability of the Rules

The Rules are drafted in relatively concise terms, and extend to just 16 paragraphs. They are intended to be applied to disputes arising from novel digital technologies, and define a digital asset to include a cryptoasset, digital token, smart contract or other digital or coded representation of an asset or transaction, and a digital asset system to mean a digital environment or platform in which a digital platform exists.

#### Incorporation and variation of the Rules

Parties may incorporate the Rules into a contract, digital asset, or digital asset system, by the insertion of a short clause, or they may otherwise adopt the Rules after a dispute has arisen. A number of variations may be made to the incorporation of the Rules, which relate to the manner in which the dispute is to be resolved. The parties may also opt to keep their identity anonymous from any person other than the tribunal.

#### A ‘rapid’ dispute resolution process

In their standard form, the Rules provide for a ‘rapid’ procedure under which the appointed arbitral tribunal is to use its best endeavours to resolve a dispute, in accordance with the law of England and Wales, within 30 days of appointment. This is in significant contrast to court based litigation which may typically proceed for 12 to 18 months before a claim is determined.

The expedited process under the Rules necessitates a truncated procedure. The claimant commences proceedings by submitting complete details of the claim to the other party and to the appointment body, including factual details, evidence and legal arguments. The respondent is then required to provide a detailed response within three days of receipt of a notice of claim. Disputes are intended to be determined without the need for a hearing.

#### Flexibility at the discretion of the tribunal

The tribunal has a significant amount of discretion in its approach to resolving disputes. From an early stage the appointed arbitrator is expected to take control of a dispute and implement a procedure that is conducive to an effective resolution. This is likely to necessitate a flexible case-by-case approach with collaborative involvement from the parties.

#### An educational charity as the arbitrator appointment body

The Society for Computers and Law (‘SCL’) is the designated appointment body under the Rules. The SCL is a registered educational charity based in the UK and its objectives include the advancement of education in relation to information technology and law.

The SCL is responsible for appointing the tribunal which is to determine a dispute under the Rules, and has a degree of flexibility in doing so. In this regard the SCL will be required to maintain a panel of arbitrators who have the relevant skills and expertise to competently take appointment. The SCL and the appointed tribunal are however not obliged to act unless the parties to the dispute have made provision for the payment of their respective fees.

#### Off-chain and on-chain implementation of arbitral decisions

The Rules allow for the tribunal to either direct a party to deal on an off-chain basis with a digital asset, or for the tribunal to itself implement a decision on an on-chain basis through the use of a private key (or other control mechanism such as a password or digital signature).

*cryptoassets.law*