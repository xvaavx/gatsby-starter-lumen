---
template: post
title: Enforcement of money judgments in relation to cryptoassets
slug: enforcement-of-money-judgments-in-relation-to-cryptoassets
socialImage: /media/image-2.jpg
draft: false
date: 2021-04-25T20:41:07.902Z
description: Equitable execution as a potential means of enforcing money
  judgments against cryptoassets.
category: Civil Claims
tags:
  - Civil Claims
  - Civil Fraud
---
The methods of enforcing a money judgment of the court include taking control of goods (bailiffs), a charging order, a third party debt order, and an attachment of earnings order.

In relation to cryptoassets of a judgment debtor, these methods are not available.

#### **Equitable execution**

A lesser-known method of enforcing money judgments is by equitable execution. In this type of application, the court has the power to make broad, wide-ranging orders. The remedy derives from Part 69 of the Civil Procedure Rules and section 37(1) of the Senior Courts Act 1981, but its application is largely governed by case law.

The appointment of an equitable receiver is a discretionary remedy of the court that will only be granted in circumstances where it is ‘just and equitable’ to do so, and is only used when other types of enforcement are not available.

#### **A potential route of enforcement against cryptoassets**

It is understood that equitable execution may be a potential route to recovery against cryptoassets. The court held in *JSC VTB Bank v Skurikhin & Ors* \[2015] EWHC 2131 (Comm) that a receiver by way of equitable execution may be appointed over whatever may be considered in equity as assets of a judgement debtor. In *Masri v Consolidated Contractors International UK Ltd and others* \[2008] EWHC 2492 (Comm) an order to appoint a receiver by way of equitable execution was granted in relation to worldwide construction contracts.

#### **Cryptoasset receiver**

The remedy involves the appointment of a receiver who is responsible for gathering in and preserving the assets of a judgment debtor. Several points of note arise in this regard:

* In ordinary circumstances the cost and complexity of appointing a receiver are significant. These issues are likely to be magnified in cases involving cryptoassets.
* Case law provides that a receiver will not be appointed where the appointment would be fruitless (*Merchant International Company Ltd v Naftogaz* \[2015] EWHC 1930 (Comm)). It is unclear what type of receiver is capable of recovering cryptoassets. Indeed, it would appear that ancillary orders or other injunctive relief would be required to have any prospect of effectiveness.
* It is beneficial to applicants that the court can appoint an equitable receiver before, or during legal proceedings.
* A receiver appointed by way of equitable execution is not the same as under the insolvency legislation, Part 25 of the Civil Procedure Rules, or under the Law of Property Act.

#### **Further points of note**

It should be noted that the applicant/judgment creditor does not receive a proprietary interest or secured status in respect to the assets that are subject to equitable execution.

An application to the court can be made without notice where there a risk that the asset that is subject to equitable execution may be dissipated, with an injunction ancillary to the application. This is likely to be pertinent in cases involving cryptoassets.

*cryptoassets.law*